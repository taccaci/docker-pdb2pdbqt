# #
# Converts a PDB file to PDBQT for use in the TACC Drug Discovery Portal
#
# 1) put a pdb file in pdb_demo directory (<yourpdb.pdb>)
# 2) docker run -it -v `pwd`/pdb_demo:/data --rm <name> /data/pdb2pdbqt.sh /data/<yourpdb.pdb>
# 3) see pdbqt file in pdb_demo/<yourpdb.pdbqt>
#
# To just see a demo, simply run:
# docker run -it -v `pwd`/pdb_demo:/data --rm <name>
# #
FROM ubuntu:12.04
MAINTAINER Stephen Mock <mock@tacc.utexas.edu>

RUN apt-get -y update && \
        apt-get -y install curl
RUN curl -O http://mgltools.scripps.edu/downloads/downloads/tars/releases/REL1.5.6/mgltools_x86_64Linux2_1.5.6.tar.gz
RUN tar xzvf mgltools_x86_64Linux2_1.5.6.tar.gz
WORKDIR /mgltools_x86_64Linux2_1.5.6
RUN ./install.sh
RUN mkdir /data
WORKDIR /data

VOLUME /data
CMD ["/data/pdb2pdbqt.sh", "/data/2FOM.pdb"]
